﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace Reload.SqlConn
{
    class DBOracleUtls
    {

        public static OracleConnection
                       GetDBConnection(string host, int port, String sid, String user, String password)
        {

            Console.WriteLine("Getting Connection ...");

            // Connection string to connect directly to Oracle.
            string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                 + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "
                 + sid + ")));Password=" + password + ";User ID=" + user;
            //   string ConnectionString = "Data Source=(DESCRIPTION =WHPROD(SOURCE_ROUTE = YES)(ADDRESS = (PROTOCOL = TCP)(HOST = luocm01.liberty.edu)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = xprd01db02-vip.liberty.edu)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME=WHPRD_USR.LIBERTY.EDU))); User Id=utl_d_pp;Password=uT1DpP;" ;

            OracleConnection conn = new OracleConnection();

            conn.ConnectionString = connString;

            return conn;
        }

    }
}