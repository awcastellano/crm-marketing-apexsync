using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Security;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.DirectoryServices.AccountManagement;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Services.Utility;
using Microsoft.Crm.Sdk.Messages;
using System.Collections.Specialized;
using Oracle.DataAccess.Client;
using Reload.SqlConn;


namespace ConsoleApplication4
{
    class Program
    {
        
      

      /*  

        static public string GetLibertyEmailFromSP(string meta, operationsLists.Lists lists)
        {
       * /
            /*This function retrieves a SharePoint user's email address based on the
             internal SharePoint user ID that is returned from the lists web service
             since it doesn't autmoatically provided the user field in a usable format
             for submitting to CRM. The lists argument of this function is a class that
             stores the connection info for SharePoint. The argument called "meta" is
             the user ID. The XML for the SharePoint lists web service call is constructed
             manually because that was easier. Most everything in the XML is case sensitive*/
        /*
            string rowLimit = "10"; //10
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument(); 
            System.Xml.XmlElement userQuery = xmlDoc.CreateElement("Query");
            System.Xml.XmlElement uviewFields = xmlDoc.CreateElement("ViewFields");
            System.Xml.XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");
            System.Xml.XmlDocument userXml = new XmlDocument();

            userQuery.InnerXml = "<Where><Eq><FieldRef Name=\"ID\" /><Value Type=\"Counter\">"+ meta+"</Value></Eq></Where>";
            uviewFields.InnerXml = "<FieldRef Name=\"EMail\" />"
            + "<FieldRef Name=\"MetaInfo\" />"
            + "<FieldRef Name=\"Title\" />"
                //"<FieldRef Name=\"Department\" />"
            + "<FieldRef Name=\"Contact_x0020_name\" />"
            + "<FieldRef Name=\"Author\" />";
            queryOptions.InnerXml = "";
            userXml.InnerXml = lists.GetListItems("UserInfo", null, userQuery, uviewFields, rowLimit, queryOptions, null).OuterXml;

            return userXml.FirstChild.FirstChild.FirstChild.Attributes.Item(0).Value;

        }
        static void Main(string[] args)
        {
         
        */
         static void Main(string[] args)
        {

        
            #region Connecting to CRM

            /*The code in this region is just standard connecting to CRM. The _serviceProxy class instance that it creates
             is used to actually call the creation of the campaigns. The _serviceProxy is also used by some of
             the supplementary functions that are used for retrieving CRM users, accounts and any other related
             entities.  There are two commented lines here. One is for connecting to crmdev instead of prod and
             the other is for choosing whether or not to use credentials of the service account or the credentials
             of the logged in user. This program does not typically run successfully against crmdev since dev lacks
             many of the users, accounts and picklist values that currently exist on production*/
           // Uri organizationUri = new Uri("https://crm.liberty.edu/LibertyUniversity/XRMServices/2011/Organization.svc");
            Uri organizationUri = new Uri("https://https://crm16dev.liberty.edu/LibertyUniversity/XRMServices/2011/Organization.svc");
            Uri homeRealmUri = null;
            ClientCredentials credentials = new ClientCredentials();
            //credentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            credentials.Windows.ClientCredential = new System.Net.NetworkCredential("crmmarketingsvc", "loosegasolineaveragemilk", "SENSENET");
            OrganizationServiceProxy _serviceProxy = new OrganizationServiceProxy(organizationUri, homeRealmUri, credentials, null);
            _serviceProxy.EnableProxyTypes();
            Console.WriteLine("Connected to CRM");
            #endregion

            #region testing
            //Console.WriteLine(GetCampaignPicklistValue("lu_printingcommoditycode", _serviceProxy, "ADPROMO"));
            //Console.ReadLine();
            #endregion

        //SEE IF YOU CAN'T PULL THIS FROM THE DB
        /*
            #region Get Last Project Number
            This program stores the SharePoint ID of the last project request that it synced in a file at 
             C:\Program Files\CRMSPSync\ProjectRequestSyncLast.txt. Since there is no installation, this file
             will have to be manually created on any machine where this program is to run. I place the program
             files in the same folder for consistency
            StreamReader reader=new StreamReader("C:\\Program Files\\CRMSPSync\\ProjectRequestSyncLast.txt");
            string LastSync = reader.ReadLine();
            reader.Dispose();
            Console.WriteLine(LastSync);
             



            #endregion  
        */
        //
        // THIS NEEDS TO BE PULLED FROM THE TABLES


       //     #region Retrieve new project requests since last run
            /*This region takes the project number that we retrieved in the last block and 
             uses it to query SharePoint for any new requests. We connect to CRM similarly
             to how we created the connection to CRM, although here we're using the credentials
             of whatever user the program is running under. This will retrieve a maximum of 
             100 items. If there are more than that, the additional items will be picked up
             in a subsequent run. Here is also where we create the lists class instance that
             is passed to the function above.
             
             Again, we create the XML for the queryr manually because it is easier. It 
             consists basically of a one section that defines the filter criteria, in this
             case items where the ID is greater than the one we retrieved from our local
             file, and another section which defines which list fields to return. Again,
             be wary of the case sensitivity of the tags in the constructed XML. The 
             results of this query are stored in the itemsXML XmlDocument which will be
             referenced later when we are creating the campaigns*/

        /*
            operationsLists.Lists lists = new operationsLists.Lists();
            lists.Credentials = System.Net.CredentialCache.DefaultCredentials;
            lists.Url = "https://sharepoint.liberty.edu/sites/OPromotionalPublications/operations/_vti_bin/Lists.asmx";
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            string listName = "{C2C5A955-5982-48C0-BF54-C79C7A2CFB56}";
            string rowLimit = "100"; //100
            
            
            System.Xml.XmlElement query = xmlDoc.CreateElement("Query");
            System.Xml.XmlElement viewFields = xmlDoc.CreateElement("ViewFields");
            
            System.Xml.XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");

            query.InnerXml = "<Where><Gt><FieldRef Name=\"ID\" />" +
            "<Value Type=\"Counter\">" + LastSync + "</Value></Gt></Where>" +
            "<OrderBy>" +
            "FieldRef Name =\"ID\" />" + 
            "</OrderBy>"
            ;
            
            viewFields.InnerXml = 
                "<FieldRef Name=\"ID\" />"
            + "<FieldRef Name=\"Email\" />"
            + "<FieldRef Name=\"Department\" />"
            + "<FieldRef Name=\"Title\" />"
            + "<FieldRef Name=\"Project_x0020_description\" />"
            + "<FieldRef Name=\"Project_x0020_Type\" />"
            + "<FieldRef Name=\"Audience\" />"
            + "<FieldRef Name=\"Text\" />"
            + "<FieldRef Name=\"Design\" />"
            + "<FieldRef Name=\"KeyMessage\" />"
            + "<FieldRef Name=\"TemplateColor\" />"
            + "<FieldRef Name=\"Requested_x0020_completion_x0020\" />"
            + "<FieldRef Name=\"fund_code\" />"
            + "<FieldRef Name=\"program_code\" />"
            + "<FieldRef Name=\"org_code\" />"
            + "<FieldRef Name=\"acct_code\" />"
            + "<FieldRef Name=\"activity_code\" />"
            + "<FieldRef Name=\"commodity_code\" />"
            + "<FieldRef Name=\"ProvostOffice\" />"
            ;
            queryOptions.InnerXml = "";
            
            XmlDocument itemsXml = new XmlDocument();

            itemsXml.InnerXml = lists.GetListItems(listName, null, query, viewFields, rowLimit, queryOptions, null).InnerXml;

            int ItemCount = Convert.ToInt32(itemsXml.ChildNodes.Item(0).Attributes.GetNamedItem("ItemCount").Value);

            int i = 0;
            Console.WriteLine("New Requests Retrieved");
            #endregion
        */
           #region getoradata
            // Connection string to connect directly to Oracle.
            /*     string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                      + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "
                      + sid + ")));Password=" + password + ";User ID=" + user;
                 OracleConnection con = new OracleConnection();
                string con.ConnectionString = "Data Source=(DESCRIPTION =WHPROD(SOURCE_ROUTE = YES)(ADDRESS = (PROTOCOL = TCP)(HOST = luocm01.liberty.edu)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = xprd01db02-vip.liberty.edu)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME=WHPRD_USR.LIBERTY.EDU)));User Id=utl_d_pp;Password=uT1DpP;" ;

                 //current
                 OracleConnection con = new OracleConnection();
                 con.ConnectionString = "User Id=utl_d_pp;Password=uT1DpP;Data Source=WHPROD";
                 con.Open();
                 Console.WriteLine("Connected to ORA");
             */
            // Get Connection. 

            OracleConnection conn = DBUtils.GetDBConnection();

            Console.WriteLine("Get Connection: " + conn);
            try
            {
                conn.Open();

                Console.WriteLine(conn.ConnectionString, "Successful Connection");
            }
            catch (Exception ex)
            {
                Console.WriteLine("## ERROR: " + ex.Message);
            //    SendEmail("## Reloader AppERROR: " + ex.Message);

                /*After every iteration, we write the Project ID that we just ran through to the local file that we retrieved from
                earlier. This way, if there is any interruption or unhandled exception, we still know exactly where we left off in
                the SharePoint list. It's sort of important that there be very little between the actual creation of the campaign
                and the updating of the file. Otherwise we open ourselves to repeated creation of the same project in the event of
                 an error.*/
                /*       using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"\\molly\4340\ADS Communications\c#apps\OracleTestApp\OracleTestApp\OracleTestAppAppErrorLog.txt"))
                       {
                           file.WriteLine(ex.Message);
                           Console.WriteLine("Last Run Updated");
                       }
                       Console.Read();
                       return;
                  */
            }

            Console.WriteLine("Connection successful!");
            OracleCommand command = conn.CreateCommand();
            //command.CommandText = "select  a.username, a.CAMPAIGNID,c.codename, c.name, (c.actualstart + 365) actualstart ,c.description,LU_MARKETINGKEYMESSAGE,LU_MARKETINGREQUIREDTEXT,LU_MARKETINGPREFERENCES,   LU_MARKETINGAUDIENCE,LU_MARKETINGSUMMARY, a.recid , e.name as dept, s.value as cmptype from utl_d_pp.reloader a  left join crm.campaignbase c on a.campaignid = c.campaignid  left join crm.campaignextensionbase b on c.campaignid = b.campaignid  left join crm.accountbase e on trim(nvl(b.lu_marketing_account,'E34C0CF1-E290-E311-9D5D-0050568800EA')) = trim(e.accountid)   left join crm.stringmapbase s on s.objecttypecode = 4400  AND s.attributename = 'typecode'  AND c.typecode = s.attributevalue where ADDED_TO_SP is null ";
            //testing
            command.CommandText = "select REQUEST_NUMBER,TYPECODE,DUEDATE,MESSAGE,CMP_NAME,LU_MASTERCAMPAIGNID,LU_MARKETINGPURPOSE,LU_MARKETING_ACCOUNT,LU_MARKETINGAUDIENCE,LU_MARKETINGKEYMESSAGE,LU_MARKETINGPONUMBER,LU_MARKETINGPREFERENCES,LU_MARKETINGREQUIREDTEXT,ACCOUNTID,DEPT, CART_ID,FILE_ID,(DESCRIPTION||'                        CONTACT INFO: '||' CONTACT EMAIL: '||CONTACT_EMAIL||' CONTACT NAME: '||CONTACT_NAME||'CONTACT PHONE: '||CONTACT_PHONE|| ' REQUEST INFO: '|| CASE WHEN TYPECODE  = 827130008 THEN ' COLORS: '||PI_COLORS||' LINK: '||PI_LINK ||' QUANITY AND SIZE: '||PI_QUANITYSIZE||' HARDWARE: '||PI_HARDWARE WHEN TYPECODE IN (2, 5, 827130001, 827130018) THEN ' PRINT SIZE: '||PRINT_SIZE||' PRINT PAGES: '||PRINT_PAGES||' PRINT QUANITY: '||PRINT_QUANITY||' PRINT USED:'||PRINT_USED||' PRINT DISPLAYED: '||PRINT_DISPLAYED||' PRINT INSTALL: '||PRINT_INSTALL ||' PRINT EVENT: '||PRINT_EVENT||' PRINT EVENT DATE: '||PRINT_EVENT_DATE||' PRINT SURFACE: '||PRINT_SURFACE||' PRINT PIECE REMVOED: '||PRINT_PIECE_REMOVED||' PRINT PIECE REMOVED DATE: '||PRINT_PIECE_REMOVED_DATE||' PRINT PERSON REMOVES '||PRINT_WHO_REMOVES WHEN TYPECODE IN (1, 827130032, 827130033) THEN ' ADVERTISEMENT SIZE: '||ADV_SIZE||' ADVERTISEMENT PUBLIC DISPLAY: '||ADV_PUBDISP||' ADVERTISEMENT EVENT: '|| ADV_EVENT||' ADVERTISEMENT EVENT DATE: '||ADV_EVENT_DATE WHEN TYPECODE IN (827130024, 827130035, 827130041) THEN ' SLIDES '||WD_SLIDES_RUN WHEN TYPECODE IN (827130000) THEN ' FROM NAME: '||FROM_NAME||' REPLY TO: '||REPLY_TO WHEN TYPECODE IN (827130005, 827130030) THEN ' CALLER ID: '||TPH_CALLERID WHEN TYPECODE IN (4) THEN 'MAILER QUANITY: '||MAIL_PROJQTY||' MAILER FCNP: '||MAIL_FCNP||' MAILER BRE: '||MAIL_BRE||' MAILER TAB OR GLUE: '||MAIL_TABGLUE||' SELF MAIL: '||MAIL_SELFMAIL||' DESIGNED ENVELOPE: '||MAIL_DESIGNED_ENV WHEN TYPECODE IN (827130024, 827130035, 827130041) THEN ' RUNTIME: '||WD_RUNTIME||' LINK: '||WD_LINK|| ' SIZE: '||WD_SIZE WHEN TYPECODE IN (827130019) THEN ' LENGTH: '||V_LENGTH||' PURPOSE: '||V_PURPOSE ||' FORMAT: '||V_FORMAT||' EVENT DATE: '||V_EVENTDATE||' EVENT LOCATION: '||V_EVENT_LOCATION||' INTENDED_USE: '||V_INTENDED_USE||' BROADCAST: '||V_BROADCAST||' STATION: '||V_STATION||' GUIDLINES: '||V_GUIDELINES ||' WEBSITE: '||V_WEBSITE||' SOCIAL MEDIA: '||V_SOCIAL_MEDIA||' SCRIPT: '||V_SCRIPT||' TALENT: '||V_TALENT||' KEY PEOPLE: '||V_KEY_PEOPLE||' VISUALS: '||V_VISUALS||' EXAMPLES: '||V_EXAMPLES||' FOOTAGE: '||V_FOOTAGE WHEN TYPECODE IN (827130029) THEN ' PREP HOURS: '||P_PREP_HOURS||' SHOOT HOURS: '||P_SHOOT_HOURS||' PROCESSING HOURS: '||P_PROCESSING_HOURS ELSE NULL END ) AS LU_DESC, CREATE_USER, CREATE_DATE, STATUS_IND from zmarketing_projectreq.requests WHERE PROPOSEDEND IS NULL  "; 
            OracleDataReader reader = command.ExecuteReader();
            Console.WriteLine("Reading DB");
            if (reader.HasRows)
            {
                while (reader.Read())
                {
/*
                    string spuid = "";
                    spuid = GetSPUserID(reader.GetOracleString(0).ToString()); //username
                    Console.WriteLine(spuid);


                    Console.WriteLine(
                        SendToSP(
                        reader.GetOracleString(1).ToString(), //campaignid
                        reader.GetOracleString(2).ToString(), //codename
                        reader.GetOracleString(3).ToString(), //name
                        reader.GetOracleDate(4).ToString(), //date
                        reader.GetOracleString(5).ToString(), //description
                        reader.GetOracleString(6).ToString(), //keymessage
                        reader.GetOracleString(7).ToString(), //reqtext
                        reader.GetOracleString(8).ToString(), //pref
                        reader.GetOracleString(9).ToString(), //aud
                        reader.GetOracleString(10).ToString(), //summary
                        spuid,
                        reader.GetOracleDecimal(11).ToString(), //recid
                        conn,
                        reader.GetOracleString(12).ToString(), //dept
                        reader.GetOracleString(13).ToString() //cmptype
                        ));
 */
                      Console.WriteLine(
                        SendToSP(
                        reader.GetOracleString(1).ToString(), //REQUEST_NUMBER 
                        reader.GetOracleString(2).ToString(), //TYPECODE 
                        reader.GetOracleString(3).ToString(), //DUEDATE 
                        reader.GetOracleString(4).ToString(), //MESSAGE 
                        reader.GetOracleString(5).ToString(), //CMP_NAME 
                        reader.GetOracleString(6).ToString(), //LU_MASTERCAMPAIGNID 
                        reader.GetOracleString(7).ToString(), //LU_MARKETINGPURPOSE 
                        reader.GetOracleString(8).ToString(), //LU_MARKETING_ACCOUNT 
                        reader.GetOracleString(9).ToString(), //LU_MARKETINGAUDIENCE 
                        reader.GetOracleString(10).ToString(), //LU_MARKETINGKEYMESSAGE 
                        reader.GetOracleString(11).ToString(), //LU_MARKETINGPONUMBER 
                        reader.GetOracleString(12).ToString(), //LU_MARKETINGPREFERENCES 
                        reader.GetOracleString(13).ToString(), //LU_MARKETINGREQUIREDTEXT 
                        reader.GetOracleString(14).ToString(), //ACCOUNTID 
                        reader.GetOracleString(15).ToString(), //DEPT 
                        reader.GetOracleString(16).ToString(), //CART_ID 
                        reader.GetOracleString(17).ToString(), //FILE_ID 
                        reader.GetOracleString(18).ToString(), //LU_DESC 
                        reader.GetOracleString(19).ToString(), //CREATE_USER 
                        reader.GetOracleString(20).ToString(), //CREATE_DATE 
                        reader.GetOracleString(21).ToString() //STATUS_IND
                        ));
                }
            }

            reader.Close();
            command.Dispose();
            conn.Close();
            #endregion
            Console.WriteLine("New Requests Retrieved");
            System.Threading.Thread.Sleep(5000);
            //Console.ReadKey();
            
        }

            /*The following while loop iterates through the itemsXML and creates CRM Campaigns based on it*/
            while (i < ItemCount)
            {
                /**/
                
                //Each of the list fields we returned are referenced by name.
               // string ID = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_ID").Value;

                //Some of the assignment values are wrapped in trys in case that field does not have a value.
                //In that case, we assign a null value and move on.

                //This If block requires that an item be a Project type in order for us to carry on with the campaign creation
                //Otherwise we skip the creation and move on to the next item in the itemsXml
              //  if (itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_ID").Value != "0")
                {
                    #region Create CRM Campaigns
                    //There's a bit of silliness with parsing the user from SharePoint and this is where we
                    //use the function we saw earlier to translate it into a meaningful string.

        /*
                    string meta = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Email").Value;
                    string libertyEmail;
                    if (meta != "")
                    {
                        meta = meta.Substring(0, meta.IndexOf(";"));
                        libertyEmail = GetLibertyEmailFromSP(meta, lists);
                    }
                    else
                    {
                        libertyEmail = null;
                    }

        


                    //Here's another try in case the Department field isn't populated in CRM

                    string Department;
                    try
                    {
                        Department = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Department").Value;
                    }
                    catch
                    {
                        Department = "";
                    }
        */
                    /*When creating the campaign, related entities must be assigned by using an entity reference. The
                     entity reference is instantiated by passing the entity type and the GUID of the entity we wish
                     to reference. Functions like GetContactFromLUEmail and GetAccountbyName are used to translate
                     the data we get from SharePoint into usable GUIDs.*/


                    EntityReference Account = new EntityReference("account", GetAccountbyName(Department, _serviceProxy));
                    //EntityReference MasterCamp = new EntityReference("campaign", ProvostOffice);
                    /*The next line creates a local instance of a Campaign that we can store all our values in. It
                    will not actually be created until we call the Create method after we've assigned it all the values
                     we got from SharePoint*/
                    Campaign newCamp = new Campaign { lu_marketing_account = Account };
                    if (libertyEmail != null)
                    {
                        try
                        {
                            newCamp.lu_MarketingRequester = new EntityReference("contact", GetContactFromLUEmail(libertyEmail, _serviceProxy));
                        }
                        catch
                        {

                        }
                    }

                    newCamp.lu_MarketingRequestID = ID;

                    //Creating some variabls to store the SharePoint field values before we attach them to our campaign
/*
                    string Description;
                    string Audience;
                    string Text;
                    string KeyMessage;
                    string Color;
                    string Design;
                    string PONumber;
                    string fund_code;
                    string program_code;
                    string org_code;
                    string activity_code;
                    string acct_code;
                    string commodity_code;
*/
                     string REQUEST_NUMBER;
                       string TYPECODE;
                       string   DUEDATE;
                       string  MESSAGE;
                        string CMP_NAME;
               string LU_MASTERCAMPAIGNID; 
               string LU_MARKETINGPURPOSE;
                string  LU_MARKETING_ACCOUNT;
                string LU_MARKETINGAUDIENCE;
              string LU_MARKETINGKEYMESSAGE;
                string LU_MARKETINGPONUMBER;
              string LU_MARKETINGPREFERENCES;
               string LU_MARKETINGREQUIREDTEXT;
                      string  ACCOUNTID;
                      string DEPT;
                    string CART_ID; 
                     string FILE_ID; 
                    string LU_DESC; 
                   string STATUS_IND;


                    Byte[] bytes = new Byte[16];
                    Guid emptyguid = new Guid(bytes);
                    ColumnSet newcolset = new ColumnSet { AllColumns = true };
                    EntityReference MasterCamp = new EntityReference();

                    Console.WriteLine(" attempting provost office info \r");
                    //Provost Office Info - Master Campaign

                    try
                    {
                        //ProvostOffice = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_ProvostOffice").Value;
                        newCamp.lu_mastercampaignid = _serviceProxy.Retrieve(Campaign.EntityLogicalName, new Guid(itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_ProvostOffice").Value), newcolset).ToEntityReference();
                        Console.WriteLine(" provost office info retrieved \r");
                    }
                    catch (NullReferenceException)
                    {
                        Console.WriteLine(" no provost office info \r");
                    }

     


                    Console.WriteLine(" continuing after provost office info \r");

                    //MasterCamp = _serviceProxy.Retrieve(Campaign.EntityLogicalName, ProvostOfficeGuid, newcolset).ToEntityReference();

                    //Another try to protect against null fields in SharePoint
                    try
                    {
                        string Title = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Title").Value;
                        newCamp.Name = Title;
                    }
                    catch (NullReferenceException)
                    {
                    }


                    /*The description in CRM is limited to 2000 characters so anything longer than that will be truncated.
                     Again, we plan for the possilibity of null data from SharePoint.*/
                    try
                    {
                        Description = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Project_x0020_description").Value;
                        Description = Description.Substring(0, Math.Min(Description.Length, 1999));
                    }
                    catch (NullReferenceException)
                    {
                        Description = "";
                    }
                    /*The GetCampaignPicklistValue function translates the textual data we get from SharePoint into
                     an OptionSetValue that CRM will understand.*/
                    try
                    {
                        string Type = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Project_x0020_Type").Value;
                        newCamp.TypeCode = new OptionSetValue(GetCampaignPicklistValue("typecode", _serviceProxy, Type));
                    }
                    catch (NullReferenceException)
                    {
                    }
                    //More of the same
                    try
                    {
                        Audience = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Audience").Value;
                    }
                    catch (NullReferenceException)
                    {
                        Audience = "";
                    }





                    try
                    {
                        Text = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Text").Value;
                    }
                    catch (NullReferenceException)
                    {
                        Text = "";
                    }

                    try
                    {
                        KeyMessage = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_KeyMessage").Value;
                    }
                    catch (NullReferenceException)
                    {
                        KeyMessage = "";
                    }


                    try
                    {
                        Color = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_TemplateColor").Value;
                    }
                    catch (NullReferenceException)
                    {
                        Color = "";
                    }

                    try
                    {
                        Design = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Design").Value;
                    }
                    catch (NullReferenceException)
                    {
                        Design = "";
                    }

                    try
                    {
                        string ProposedStart = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_Requested_x0020_completion_x0020").Value;
                        newCamp.ProposedStart = Convert.ToDateTime(ProposedStart);
                    }
                    catch (NullReferenceException)
                    {
                    }

                    try
                    {
                        PONumber = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_PO_Number").Value;
                        newCamp.lu_MarketingPONumber = PONumber;
                    }
                    catch (NullReferenceException)
                    {
                    }

                    try
                    {
                        fund_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_lu_fund").Value;
                        newCamp.lu_Fund = fund_code;
                    }
                    catch (NullReferenceException)
                    {
                    }
                    try
                    {
                        program_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_program_code").Value;
                        newCamp.lu_Program = program_code;
                    }
                    catch (NullReferenceException)
                    {
                    }
                    try
                    {
                        org_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_org_code").Value;
                        newCamp.lu_Organization = org_code;
                    }
                    catch (NullReferenceException)
                    {
                    }
                    try
                    {
                        activity_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_activity_code").Value;
                        newCamp.lu_Activity = activity_code;
                    }
                    catch (NullReferenceException)
                    {
                    }
                    try
                    {
                        acct_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_acct_code").Value;
                        newCamp.lu_PrintingAccount = acct_code;
                    }
                    catch (NullReferenceException)
                    {
                    }
                    try
                    {
                        commodity_code = itemsXml.ChildNodes.Item(0).ChildNodes.Item(i).Attributes.GetNamedItem("ows_commodity_code").Value;
                        Console.WriteLine(commodity_code);
                        newCamp.lu_PrintingCommodityCode = new OptionSetValue(GetCampaignPicklistValue("lu_printingcommoditycode", _serviceProxy, commodity_code));
                    }
                    catch (NullReferenceException)
                    {
                    }
                    newCamp.lu_MarketingAudience = Audience;
                    newCamp.Description = Description;
                    newCamp.lu_MarketingRequiredText = Text;
                    newCamp.lu_MarketingKeyMessage = KeyMessage;
                    //I didn't see a field for Color and design so I stuck them in lu_MarketingPreferences. 
                    //So far no complaints.
                    newCamp.lu_MarketingPreferences = Color + "\n\n\r" + Design;

                    /*This is where our local instance of the campaign is actually created in CRM.
                     If CRM is being slow this operation can timeout. If that happens, we simply retry the
                     create operation. */
                    Guid newcampguid;
                Create:
                    try
                    {

                        newcampguid = _serviceProxy.Create(newCamp);
                    }
                    catch (TimeoutException)
                    {
                        goto Create;
                    }

                    #endregion

                    #region write SyncLast ID to file

                    Console.WriteLine("Campaign Created");

                    /*After every iteration, we write the Project ID that we just ran through to the local file that we retrieved from
                    earlier. This way, if there is any interruption or unhandled exception, we still know exactly where we left off in
                    the SharePoint list. It's sort of important that there be very little between the actual creation of the campaign
                    and the updating of the file. Otherwise we open ourselves to repeated creation of the same project in the event of
                     an error.*/
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\\Program Files\\CRMSPSync\\ProjectRequestSyncLast.txt"))
                    {
                        file.WriteLine(ID);
                        Console.WriteLine("Last Run Updated");
                    }
                    #endregion

                    //kph
       /* 
            #region Update "Sent To CRM" flag in SP 

                    XmlDocument spupdatedoc = new XmlDocument();

                    string buildXml = string.Format("<Batch OnError=\"Continue\" ListVersion=\"1\" viewName=\"1B6896BF-276C-48B6-8514-50D96F3AA87E\"> <Method ID=\"1\" Cmd=\"Update\"> <Field Name=\"ID\">{0}</Field> <Field Name=\"Sent_x0020_To_x0020_CRM\">true</Field> </Method> </Batch>",ID);

                    Console.WriteLine(buildXml); //check the string

                    spupdatedoc.LoadXml(buildXml); // add string to batch

                    lists.UpdateListItems("C2C5A955-5982-48C0-BF54-C79C7A2CFB56", spupdatedoc); //excecute batch update of 1 record
                    
                    #endregion //kph
         */

//UPDATE REQUEST
                   static public void updateUtlRldr(OracleConnection con, int newspid)
                    {
                        OracleCommand cmd = con.CreateCommand();
                        cmd.CommandText = "UPDATE ZMARKETING_PROJECTREQ.REQUESTS SET PROPOSEDEND = SYSDATE WHERE REQUEST_NUMBER =" + newspid + "'";
                        //testing
                        //cmd.CommandText = "UPDATE UTL_D_PP.RELOADER_1 SET ADDED_TO_SP = SYSDATE, SPID = '" + newspid + "' WHERE RECID = '" + recid + "'";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }                           

                    #region Retrieve and Upload Attachments
                    /*This region is where we download the attachments from SharePoint and upload them to CRM. Note that the
                          Campaign has already been created so if we fail here the only consequence is that the attachment
                          won't be created. The sync will still continue creating subsequent requests and since we already
                          updated the last run we don't run the risk of repeatedly uploading the same attachment. */
                    try
                    {
                        //We're using the same web service to retrieve the xml list of attachments as we did to retrieve the project requests
                        XmlNode attXml = lists.GetAttachmentCollection(listName, ID);
                        //See how many projects there are set up the iterator/
                        int attCount = attXml.ChildNodes.Count;
                        int k = 0;
                        while (k < attCount)
                        {
                            string filepath = attXml.ChildNodes.Item(k).InnerXml;
                            string filename = filepath.Substring(filepath.LastIndexOfAny("/".ToCharArray()));
                            //the buffer byte array is where we'll temporarily store the attachments before we upload them
                            byte[] buffer;

                            using (WebClient Client = new WebClient())
                            {
                                //the use of default credentials again means we'll have the permissions of whatever account lanched the program
                                Client.UseDefaultCredentials = true;
                                buffer = Client.DownloadData(filepath);
                            }

                            /*Here we instantiate the Annotationt that will hold the attachment and set the file name.
                              Note that we need the guid from when we created the campaign we created earlier to be the object id.*/
                            Annotation attachment = new Annotation();
                            attachment.ObjectId = new EntityReference(Campaign.EntityLogicalName, newcampguid);
                            attachment.FileName = filename;
                            attachment.DocumentBody = Convert.ToBase64String(buffer);

                            /*We retry to create the attachment in the event of a timeout. All other exceptions are written to a log.
                             This way if there is a failure we can investigate it while the sync continues to bring over project requests.*/
                        CreateAttachment:
                            try
                            {
                                _serviceProxy.Create(attachment);
                            }
                            catch (TimeoutException)
                            {
                                goto CreateAttachment;
                            }
                            catch (Exception e)
                            {


                                using (System.IO.StreamWriter errorlog = File.AppendText(@"C:\\Program Files\\CRMSPSync\\AttachmentErrorLog.txt"))
                                {
                                    errorlog.WriteLine(" Attachment for {0} not created.", newcampguid);
                                    errorlog.WriteLine(filepath);
                                    errorlog.WriteLine(e.Data);
                                    errorlog.WriteLine(e.InnerException);
                                    errorlog.WriteLine(e.Message);

                                }
                            }

                            k++;

                        }

                    }
                    //just another chance to write possible errors in this block to the errorlog.
                    catch (Exception e)
                    {
                        try
                        {
                            using (System.IO.StreamWriter errorlog = File.AppendText(@"C:\\Program Files\\CRMSPSync\\AttachmentErrorLog.txt"))
                            {
                                errorlog.WriteLine(" Attachment for {0} not created.", newcampguid);
                                errorlog.WriteLine(e.Data);
                                errorlog.WriteLine(e.InnerException);
                                errorlog.WriteLine(e.Message);

                            }
                        }
                        catch
                        {
                        }

                    }
                    #endregion

                }
                else
                {
                    /*After every iteration, we write the Project ID that we just ran through to the local file that we retrieved from
                  earlier. This way, if there is any interruption or unhandled exception, we still know exactly where we left off in
                  the SharePoint list. It's sort of important that there be very little between the actual creation of the campaign
                  and the updating of the file. Otherwise we open ourselves to repeated creation of the same project in the event of
                  an error.*/
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\\Program Files\\CRMSPSync\\ProjectRequestSyncLast.txt"))
                    {
                        file.WriteLine(ID);
                        Console.WriteLine("Last Run Updated");
                        
                    }
                }
                
                //increment the iterator to move to the next project in the itemsXML
                i++;

  
                
            }

            //Console.ReadKey(); //keep the window open


        }

      


        static public Guid GetContactFromLUEmail(string userid, OrganizationServiceProxy serviceProxy)
        {
            /*this function retrieves a contact GUID given a liberty.edu email address. We only take the first matching record.*/
            Console.WriteLine(userid);
            ConditionExpression condition1 = new ConditionExpression();

            //liberty emails are almost exclusively stored in the emailaddress1 field so that is where we are looking
            condition1.AttributeName = "emailaddress1";
            condition1.Operator = ConditionOperator.Equal;
            condition1.Values.Add(userid);

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);
            
            QueryExpression query = new QueryExpression("contact");

            //firstname and lastname are retrieved for troubleshooting, but not returned by the function.
            //Only the GUID is returned.
            query.ColumnSet.AddColumns("firstname", "lastname", "contactid");
            query.Criteria.AddFilter(filter1);
            EntityCollection result1 = serviceProxy.RetrieveMultiple(query);

            //We potentially could have just retrieved multiple contacts, but we assume the first one is the one we want.
            //If it didn't return anything we proceed to look in the emailaddress2 and emailaddress3 fields.
            Entity newEntity;
            try
            {
                newEntity = result1.Entities[0];
            }
            catch
            {
                newEntity = null;
            }
            if(newEntity == null)
            {
                condition1.AttributeName = "emailaddress2";
                result1 = serviceProxy.RetrieveMultiple(query);
            }
            try
            {
                newEntity = result1.Entities[0];
            }
            catch
            {
                newEntity = null;
            }
            if (newEntity == null)
            {
                condition1.AttributeName = "emailaddress3";
                result1 = serviceProxy.RetrieveMultiple(query);
            }
            try
            {
                newEntity = result1.Entities[0];
            }
            catch
            {
                newEntity = null;
            }
            return newEntity.GetAttributeValue<Guid>("contactid");

        }
        static public int GetCampaignPicklistValue(string entityLogName, OrganizationServiceProxy serviceProxy, string searchString)
        {
            //function returns the internal picklist value id give the name of the picklist attribute and the value we want to match.
            //Curently hardcoded for campaigns. Can be extended to work for other Entities 
            //by passing the class entitylogicalname as a parameter.
            //Note that we are again passing the serviceProxy as an argument to the function so that it can be used by it.

            RetrieveAttributeRequest retrieveAttributeRequest =
            new RetrieveAttributeRequest { EntityLogicalName = Campaign.EntityLogicalName, LogicalName = entityLogName, RetrieveAsIfPublished = true };

            //The next line is where we actually retrieve the picklist values from CRM
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)serviceProxy.Execute(retrieveAttributeRequest);

            //Extracting the PicklistAttributeMetadata from the response.
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;

            //We stuff the picklist values and keys into a list and iterate through them until we find the one
            //that matches the string we passed to the function.
            var returnList = new List<KeyValuePair<string, int>>();
            var rlist = retrievedPicklistAttributeMetadata.OptionSet.Options;

            int i = 0;
            while (i < rlist.Count)
            {
                returnList.Add(new KeyValuePair<string, int>(rlist[i].Label.UserLocalizedLabel.Label, rlist[i].Value.Value));
                i++;
            }

            //-1 represents an unfilled picklist attribute. If we find a match we'll change it to the matching value
            //but if not we still have a valid value to return.
            int pickListValue = -1;

            foreach (KeyValuePair<string, int> kvp in returnList)
            {
                if (kvp.Key == searchString)
                {
                    pickListValue = kvp.Value;
                    break;
                }
                else
                {
                    pickListValue = -1;

                }
            }

            return pickListValue;

        }
        static public Guid GetAccountbyName(string accountname, OrganizationServiceProxy serviceProxy)
        {
            //function retrieves account guids given an account name. Only the first matching name is retrieved.
            //This function operates almost identically to the GetContact function, but uses the account (department)
            //name as its criteria instead of a liberty email and obviously retrieves an account GUID instead
            //of a Contact.
            ConditionExpression condition1 = new ConditionExpression();

            //looking for exact match on name
            condition1.AttributeName = "name";
            condition1.Operator = ConditionOperator.Equal;
            condition1.Values.Add(accountname);

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);
            QueryExpression query = new QueryExpression("account");

            //we return the account guid
            query.ColumnSet.AddColumns("accountid");
            query.Criteria.AddFilter(filter1);
            EntityCollection result1 = serviceProxy.RetrieveMultiple(query);

            //We only retrieve the first matching record
            Entity newEntity;

            try
            {
                newEntity = result1.Entities[0];
            }
            catch
            {
                newEntity = null;
            }

            //The program will fail here if we didn't find a matching Department/Account which
            //is what we want since that probably indicates an erroneous or missing Account in CRM
            //and it's better to fix that and rerun the application from there instead of failing to fill
            //in the department. 

            return newEntity.GetAttributeValue<Guid>("accountid");

        }


        static public Guid GetMasterCampGUID(string ProvostOffice, OrganizationServiceProxy serviceProxy)
        {
            //function retrieves master account by guid from SP. Only the first matching name is retrieved.
            //This function operates almost identically to the GetContact function, but uses the account (department)
            //name as its criteria instead of a liberty email and obviously retrieves an account GUID instead
            //of a Contact.
            ConditionExpression condition1 = new ConditionExpression();

            //looking for exact match on name
            condition1.AttributeName = "campaignid";
            condition1.Operator = ConditionOperator.Equal;
            condition1.Values.Add(ProvostOffice);

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);
            QueryExpression query = new QueryExpression("campaign");

            //we return the account guid
            query.ColumnSet.AddColumns("campaignid");
            query.Criteria.AddFilter(filter1);
            EntityCollection result1 = serviceProxy.RetrieveMultiple(query);

            //We only retrieve the first matching record
            Entity newEntity;

            try
            {
                newEntity = result1.Entities[0];
            }
            catch
            {
                newEntity = null;
            }

            //The program will fail here if we didn't find a matching Department/Account which
            //is what we want since that probably indicates an erroneous or missing Account in CRM
            //and it's better to fix that and rerun the application from there instead of failing to fill
            //in the department. 

            return newEntity.GetAttributeValue<Guid>("campaignid");

        }

        

    }

}
