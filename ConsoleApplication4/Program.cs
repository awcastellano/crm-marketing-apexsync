﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Security;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.DirectoryServices.AccountManagement;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Services.Utility;
using Microsoft.Crm.Sdk.Messages;
using System.Collections.Specialized;
using Oracle.DataAccess.Client;
using Reload.SqlConn;
using System.Threading.Tasks;
//using Oracle.ManagedDataAccess.Client; //

namespace ConsoleApplication4
{
    class Program
    {
       
         static void Main(string[] args)
        {
                 #region getoradata

            OracleConnection conn = DBUtils.GetDBConnection();

            Console.WriteLine("Get Connection: " + conn);
            try
            {
                conn.Open();

                Console.WriteLine(conn.ConnectionString, "Successful Connection");
            }
            catch (Exception ex)
            {
                Console.WriteLine("## ERROR: " + ex.Message);
            }

            Console.WriteLine("Connection successful!");
            /*
   
                  OracleCommand command = conn.CreateCommand();
                 //command.CommandText = "select  a.username, a.CAMPAIGNID,c.codename, c.name, (c.actualstart + 365) actualstart ,c.description,LU_MARKETINGKEYMESSAGE,LU_MARKETINGREQUIREDTEXT,LU_MARKETINGPREFERENCES,   LU_MARKETINGAUDIENCE,LU_MARKETINGSUMMARY, a.recid , e.name as dept, s.value as cmptype from utl_d_pp.reloader a  left join crm.campaignbase c on a.campaignid = c.campaignid  left join crm.campaignextensionbase b on c.campaignid = b.campaignid  left join crm.accountbase e on trim(nvl(b.lu_marketing_account,'E34C0CF1-E290-E311-9D5D-0050568800EA')) = trim(e.accountid)   left join crm.stringmapbase s on s.objecttypecode = 4400  AND s.attributename = 'typecode'  AND c.typecode = s.attributevalue where ADDED_TO_SP is null ";
                 //testing
                 command.CommandText = "select REQUEST_NUMBER spuid, TYPECODE,DUEDATE,MESSAGE,CMP_NAME,LU_MASTERCAMPAIGNID, LU_MARKETINGPURPOSE,LU_MARKETING_ACCOUNT,LU_MARKETINGAUDIENCE,LU_MARKETINGKEYMESSAGE,LU_MARKETINGPONUMBER,LU_MARKETINGPREFERENCES,LU_MARKETINGREQUIREDTEXT,ACCOUNTID,DEPT, CART_ID,FILE_ID,(DESCRIPTION||'                        CONTACT INFO: '||' CONTACT EMAIL: '||CONTACT_EMAIL||' CONTACT NAME: '||CONTACT_NAME||'CONTACT PHONE: '||CONTACT_PHONE|| ' REQUEST INFO: '|| CASE WHEN TYPECODE  = 827130008 THEN ' COLORS: '||PI_COLORS||' LINK: '||PI_LINK ||' QUANITY AND SIZE: '||PI_QUANITYSIZE||' HARDWARE: '||PI_HARDWARE WHEN TYPECODE IN (2, 5, 827130001, 827130018) THEN ' PRINT SIZE: '||PRINT_SIZE||' PRINT PAGES: '||PRINT_PAGES||' PRINT QUANITY: '||PRINT_QUANITY||' PRINT USED:'||PRINT_USED||' PRINT DISPLAYED: '||PRINT_DISPLAYED||' PRINT INSTALL: '||PRINT_INSTALL ||' PRINT EVENT: '||PRINT_EVENT||' PRINT EVENT DATE: '||PRINT_EVENT_DATE||' PRINT SURFACE: '||PRINT_SURFACE||' PRINT PIECE REMVOED: '||PRINT_PIECE_REMOVED||' PRINT PIECE REMOVED DATE: '||PRINT_PIECE_REMOVED_DATE||' PRINT PERSON REMOVES '||PRINT_WHO_REMOVES WHEN TYPECODE IN (1, 827130032, 827130033) THEN ' ADVERTISEMENT SIZE: '||ADV_SIZE||' ADVERTISEMENT PUBLIC DISPLAY: '||ADV_PUBDISP||' ADVERTISEMENT EVENT: '|| ADV_EVENT||' ADVERTISEMENT EVENT DATE: '||ADV_EVENT_DATE WHEN TYPECODE IN (827130024, 827130035, 827130041) THEN ' SLIDES '||WD_SLIDES_RUN WHEN TYPECODE IN (827130000) THEN ' FROM NAME: '||FROM_NAME||' REPLY TO: '||REPLY_TO WHEN TYPECODE IN (827130005, 827130030) THEN ' CALLER ID: '||TPH_CALLERID WHEN TYPECODE IN (4) THEN 'MAILER QUANITY: '||MAIL_PROJQTY||' MAILER FCNP: '||MAIL_FCNP||' MAILER BRE: '||MAIL_BRE||' MAILER TAB OR GLUE: '||MAIL_TABGLUE||' SELF MAIL: '||MAIL_SELFMAIL||' DESIGNED ENVELOPE: '||MAIL_DESIGNED_ENV WHEN TYPECODE IN (827130024, 827130035, 827130041) THEN ' RUNTIME: '||WD_RUNTIME||' LINK: '||WD_LINK|| ' SIZE: '||WD_SIZE WHEN TYPECODE IN (827130019) THEN ' LENGTH: '||V_LENGTH||' PURPOSE: '||V_PURPOSE ||' FORMAT: '||V_FORMAT||' EVENT DATE: '||V_EVENTDATE||' EVENT LOCATION: '||V_EVENT_LOCATION||' INTENDED_USE: '||V_INTENDED_USE||' BROADCAST: '||V_BROADCAST||' STATION: '||V_STATION||' GUIDLINES: '||V_GUIDELINES ||' WEBSITE: '||V_WEBSITE||' SOCIAL MEDIA: '||V_SOCIAL_MEDIA||' SCRIPT: '||V_SCRIPT||' TALENT: '||V_TALENT||' KEY PEOPLE: '||V_KEY_PEOPLE||' VISUALS: '||V_VISUALS||' EXAMPLES: '||V_EXAMPLES||' FOOTAGE: '||V_FOOTAGE WHEN TYPECODE IN (827130029) THEN ' PREP HOURS: '||P_PREP_HOURS||' SHOOT HOURS: '||P_SHOOT_HOURS||' PROCESSING HOURS: '||P_PROCESSING_HOURS ELSE NULL END ) AS LU_DESC, CREATE_USER, CREATE_DATE, STATUS_IND from zmarketing_projectreq.requests WHERE PROPOSEDEND IS NULL  "; 
                 OracleDataReader reader = command.ExecuteReader();
                 Console.WriteLine("Reading DB");
                 if (reader.HasRows)
                 {
                     while (reader.Read())
                     {

                           Console.WriteLine(
                             SendToCRM(
                             reader.GetOracleString(1).ToString(), //REQUEST_NUMBER 
                             reader.GetOracleString(2).ToString(), //TYPECODE 
                             reader.GetOracleString(3).ToString(), //DUEDATE 
                             reader.GetOracleString(4).ToString(), //MESSAGE 
                             reader.GetOracleString(5).ToString(), //CMP_NAME 
                             reader.GetOracleString(6).ToString(), //LU_MASTERCAMPAIGNID 
                             reader.GetOracleString(7).ToString(), //LU_MARKETINGPURPOSE 
                             reader.GetOracleString(8).ToString(), //LU_MARKETING_ACCOUNT 
                             reader.GetOracleString(9).ToString(), //LU_MARKETINGAUDIENCE 
                             reader.GetOracleString(10).ToString(), //LU_MARKETINGKEYMESSAGE 
                             reader.GetOracleString(11).ToString(), //LU_MARKETINGPONUMBER 
                             reader.GetOracleString(12).ToString(), //LU_MARKETINGPREFERENCES 
                             reader.GetOracleString(13).ToString(), //LU_MARKETINGREQUIREDTEXT 
                             reader.GetOracleString(14).ToString(), //ACCOUNTID 
                             reader.GetOracleString(15).ToString(), //DEPT 
                             reader.GetOracleString(16).ToString() //CART_ID 
                     //        reader.GetOracleString(17).ToString(), //FILE_ID 
                     //        reader.GetOracleString(18).ToString(), //LU_DESC 
                     //        reader.GetOracleString(19).ToString(), //CREATE_USER 
                     //        reader.GetOracleString(20).ToString(), //CREATE_DATE 
                     //        reader.GetOracleString(21).ToString() //STATUS_IND
                             ));
                     }
                 }

            
                 reader.Close();
            command.Dispose();
              */
            conn.Close();
            #endregion
            Console.WriteLine("New Requests Retrieved");
            System.Threading.Thread.Sleep(5000);
            //Console.ReadKey();
            
        }
        
       static public void ConnectToCRM()
        {
         
            #region Connecting to CRM

            /*The code in this region is just standard connecting to CRM. The _serviceProxy class instance that it creates
             is used to actually call the creation of the campaigns. The _serviceProxy is also used by some of
             the supplementary functions that are used for retrieving CRM users, accounts and any other related
             entities.  There are two commented lines here. One is for connecting to crmdev instead of prod and
             the other is for choosing whether or not to use credentials of the service account or the credentials
             of the logged in user. This program does not typically run successfully against crmdev since dev lacks
             many of the users, accounts and picklist values that currently exist on production*/
           // Uri organizationUri = new Uri("https://crm.liberty.edu/LibertyUniversity/XRMServices/2011/Organization.svc");
            Uri organizationUri = new Uri("https://crm16dev.liberty.edu/LibertyUniversity/XRMServices/2011/Organization.svc");
               Uri homeRealmUri = null;
            ClientCredentials credentials = new ClientCredentials();
            //credentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            credentials.Windows.ClientCredential = new System.Net.NetworkCredential("crmmarketingsvc", "loosegasolineaveragemilk", "SENSENET");
            OrganizationServiceProxy _serviceProxy = new OrganizationServiceProxy(organizationUri, homeRealmUri, credentials, null);
            _serviceProxy.EnableProxyTypes();
            Console.WriteLine("Connected to CRM");
            #endregion

        }

        

    }

}
